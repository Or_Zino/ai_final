#pragma once
#include <iostream>
#include <string>

const int MAX_AMMO = 210;
const int MIN_AMMO = 50;
const int MAX_HP = 100;
const int MIN_HP = 20;

using namespace std;
class Character // Every char is spawned with max hp and ammo
{

private:
	string name;
	int hp = MAX_HP;
	int ammoCap = MAX_AMMO;
	string myTeam;
	bool isFighting;

public:
	Character(string myName, string team){}
	bool IsSameTeam(Character c){}
	int GetHP() { return hp; }
	int GetAmmo() { return ammoCap; }
	void Move(){}
	void SearchAndDestroy(){}
	void Survive(){}
	void FillAmmo(){}
	void FillHP(){}
	void GiveSupply(){}
	void ShortRangeGrenade(){}
	void LongRangeShoot(){}
};

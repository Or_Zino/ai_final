#pragma once
#include "Bullet.h"
#include "Room.h"
const int NUM_BULLETS = 20;

class Grenade
{
private:
	Bullet* bullets[NUM_BULLETS];
	double cx, cy;
	bool isExploded;
public:
	Grenade(double x, double y);
	void Explode();
	bool IsExploded() { return isExploded; }
	void Exploding(int maze[MSZ][MSZ]);
	void DrawMe();
	void UpdateSecurityMap(int maze[MSZ][MSZ],
		double security_map[MSZ][MSZ], double delta);
};


#include "Potion.h"
#include <stdlib.h>
#include "glut.h"


Potion::Potion()
{
}

Potion::Potion(double cx, double cy)
{
	x = cx;
	y = cy;
}

void Potion::DrawP()
{
	glColor3d(1, 1, 1); //green
	glBegin(GL_POLYGON);
	glVertex2d(x, y + 0.05);
	glVertex2d(x + 0.05, y);
	glVertex2d(x, y - 0.05);
	glVertex2d(x - 0.05, y);
	glEnd();
}

void Potion::setDisplay(bool Display)
{
	isDisplay = Display;
}


#include "Room.h"
#include "glut.h"
#include "Potion.h"
#include "Ammo.h"
#include <math.h>
#include <vector>
//#include <bits/stdc++.h>
using namespace std;

Room::Room()
{
	width = 0;
	height = 0;
	centerX = 0;
	centerY = 0;
}

void Room::DrawMe()
{
	glColor3d(1, 1, 1); // white
	glBegin(GL_POLYGON);
	glVertex2d(0.5, 0.2);
	glVertex2d(0.8, 0.2);
	glVertex2d(0.8, -0.1);
	glVertex2d(0.5, -0.1);
	glEnd();
}

void Room::FillMaze(int maze[MSZ][MSZ])
{
	int i, j;

	for (i = centerY - height / 2; i < centerY + height / 2; i++)
		for (j = centerX - width / 2; j < centerX + width / 2; j++)
			maze[i][j] = SPACE;
}

bool Room::IsOverlap(int w, int h, int row, int col)
{
	int hdist, vdist; // horizontal and vertical distances between rooms centers

	hdist = abs(col - centerX);
	vdist = abs(row - centerY);
	return hdist < w / 2 + width / 2 + 2 && vdist < h / 2 + height / 2 + 2;
}

//Potion Room::FillRoomP(double row, double col) <- needs to be
int Room::FillRoomP(double row, double col)
{
	Potion x;
	int p = 5;
	return p;
	//bool PotionDisplay = true;
	//bool AmmoDisplay = true;
	//if (PotionDisplay)
	//{
		//p.DrawP();
	//}
}
//Ammo Room::FillRoomA(double x, double y)
//{
//	Ammon a(x + 1.5, y + 1.5);
//	//if (AmmoDisplay)
//	{
//		a.DrawA();
//	}
//	return a;
//}
#include "room.h"
#include "Character.h"
#include <iostream>
#include <string>

Character::Character(string myName, string team)
{
	name = myName;
	hp = MAX_HP;
	ammoCap = MAX_AMMO;
	myTeam = team;
	isFighting = false;
}

bool Character::IsSameTeam(Character c)
{
	
	return this->myTeam == c.myTeam;
}

void Character::Survive()
{
	if (ammoCap <= MIN_AMMO)
	{
		cout << name << " is going to fill ammo !";
		isFighting = false;
		FillAmmo();
	}
	if (hp <= MIN_HP)
	{
		cout << name << " is going to fill HP !";
		isFighting = false;
		FillHP();
	}
}

void Character::Move()
{
	
}

void Character::SearchAndDestroy()
{

}

void Character::FillAmmo()
{

}

void Character::FillHP()
{

}

void Character::GiveSupply()
{

}

void Character::ShortRangeGrenade()
{

}

void Character::LongRangeShoot()
{

}

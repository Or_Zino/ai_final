#pragma once

class Node
{
private:
	int row, col;
	Node* parent;
	double g, h, f;
public:
	Node();
	Node(int r, int c, int tr, int tc, double g, Node* p);
	double Distance(int r1, int c1, int r2, int c2);
	void SetRow(int r) { row = r; };
	void SetCol(int c) { col = c; };
	int GetRow() { return row; };
	int GetCol() { return col; };
	Node* GetParent() { return parent; };
	double GetF() { return f; };
	double GetG() { return g; };
	double GetH() { return h; };
	bool operator == (const Node &other) 
	{
		return row == other.row && col == other.col;
	}
};


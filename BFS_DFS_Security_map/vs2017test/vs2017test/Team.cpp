#include "room.h"
#include "Team.h"
#include <iostream>
#include <string>

const int NUM_OF_PLAYERS = 2;

Team::Team(string myName) {
	teamName = myName;
	tColor = TEAM_COLORS.find(teamName)->second;
	const string names[2] = { (teamName == "Alliance") ? ALLIANCE_NAME : HORDE_NAMES };
	for (int i = 0; i < NUM_OF_PLAYERS; i++)
	{
		characters[i] = new Character(names[i], teamName);
	}
}

string Team::GetTeam()
{
	return teamName;
}
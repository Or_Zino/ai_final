#pragma once
#include "room.h"
const double SPEED = 0.01;

class Bullet
{
private:
	double x, y;
	double dirx, diry;
	bool isMoving;
public:
	Bullet(double cx, double cy, double dx, double dy);
	Bullet(double cx, double cy);
	void DrawMe();
	void Fire(bool f) { isMoving = f; }
	bool IsMoving() { return isMoving; }
	void Move(int maze[MSZ][MSZ]);
	double GetX() { return x; }
	double GetY() { return y; }
	void UpdateMap(int maze[MSZ][MSZ], double security_map[MSZ][MSZ], double delta);
};


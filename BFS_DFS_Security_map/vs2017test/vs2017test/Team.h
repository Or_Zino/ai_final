#pragma once
#include <iostream>
#include <string>
#include <map>
#include "room.h"
#include "Character.h"

using std::string;
using namespace std;

const int NUM_OF_PLAYERS = 2;
const string HORDE_NAMES[2] = { "Troll", "Orc" };
const string ALLIANCE_NAME[2] = { "Elf", "Gnome" };
string NAMES[2];

const map<string, string> TEAM_COLORS {
		{"Alliance", "Green"},
		{"Horde", "Red"},
};


class Team
{
private:
	string teamName;
	string tColor;
	Character* characters[NUM_OF_PLAYERS];

public:
	Team(string myName) {
		teamName = myName;
		tColor = TEAM_COLORS.find(teamName)->second;
		/*if (teamName == "Alliance")
		{
			NAMES[0] = ALLIANCE_NAME[0];
			NAMES[1] = ALLIANCE_NAME[1];
		}
		else {
			NAMES[0] = HORDE_NAMES[0];
			NAMES[1] = HORDE_NAMES[1];
		}*/
		for (int i = 0; i < NUM_OF_PLAYERS; i++)
		{
			characters[i] = new Character(NAMES[i], "Alliance");
		}
	}
};
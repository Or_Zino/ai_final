#include "glut.h"
#include <time.h>
#include <vector>
#include "Cell.h"
#include <iostream>
#include "Room.h"
#include "Node.h"
#include <queue>
#include "CompareNodes.h"
#include "EqualNodes.h"
#include "Bullet.h"
#include "Grenade.h"
#include "Potion.h"
#include "Ammo.h"
#include "Team.h"
#include <string>
#include <map>

using std::string;
using namespace std;

const int WIDTH = 800;
const int HEIGHT = 800;

//const int MSZ = 100; // it is defined in Room.h
const int NUM_ROOMS = 12;

int maze[MSZ][MSZ] = { 0 }; // 0 is SPACE
double security_map[MSZ][MSZ] = { 0 };// danger map: 0 means no danger
Room rooms[NUM_ROOMS];

Bullet* pb = nullptr;
Grenade* pg = nullptr;
Team* alliance = nullptr;
vector <Potion> pp;
vector <Ammo> pa;

void InitMaze();
void InitRooms();
void DigTunnels();

void init()
{
	glClearColor(0.8, 0.7, 0.5, 0);// color of window background
	glOrtho(-1, 1, -1, 1, 1, -1);

	srand(time(0));

	InitMaze();
	InitRooms();
	DigTunnels();
	
}

// init the whole space with WALLS
void InitMaze()
{
	int i, j;

	for(i=0;i<MSZ;i++)
		for (j = 0; j < MSZ; j++)
					maze[i][j] = WALL;
}

void InitRooms()
{
	int i,j;
	int w, h, row, col;
	int min = 10;
	double x , y;
	bool overlap;

	for (i = 0; i < NUM_ROOMS; i++)
	{
		do {
			overlap = false;
			w = min + rand() % 12;
			h = min + rand() % 12;
			col = 2 + w / 2 + rand() % (MSZ - w - 4);
			row = 2 + h / 2 + rand() % (MSZ - h - 4);
			x = row + 2.5;
			y = col + 2.5;
			// check if the above definitions don't make a room to overlap with any other room
			for (j = 0; j < i && !overlap; j++)
				if (rooms[j].IsOverlap(w, h, row, col))
					overlap = true;
			
		}
		while (overlap);

		rooms[i].SetWidth(w);
		rooms[i].SetHeigth(h);
		rooms[i].SetCenterX(col);
		rooms[i].SetCenterY(row);
		rooms[i].FillMaze(maze);
		rooms[i].FillRoomP(x, y);
		//rooms[i].FillRoomA(x, y);

	}
	
	
}

// the Node pn is already in pq. we need to replace it with a new copy (pn) because
// the new copy has a better f 
void UpdatePQ(priority_queue <Node, vector<Node>, CompareNodes> &pq, Node* pn)
{
	vector <Node> tmp;
	Node best=pq.top();
	
	while (!(best.operator==(*pn) && !pq.empty()))
	{
		pq.pop();
		tmp.push_back(best);
		best = pq.top();
	}

	// we either found pn in pq or pq got empty
	if (!pq.empty())
	{
		pq.push(*pn);
	}

	while (!tmp.empty())
	{
		best = tmp.at(tmp.size()-1);
		pq.push(best);
		tmp.pop_back();
	}
}

// row, col are the coordinates of a new Node,
// trow, tcol are the coordinates of a target
void CheckNeighbor(Node* pcurrent, int row, int col, int trow, int tcol,
	vector <Node> &grays, vector <Node> &blacks, 
	priority_queue <Node, vector<Node>, CompareNodes> &pq)
{
	Node* pneighbor;
	double space_cost = 0.1,wall_cost = 1.5,cost;
	vector <Node>::iterator it_gray;
	vector <Node>::iterator it_black;


	// the cost of motion to the new Node depends on whather it is SPACE or WALL
	if (maze[row][col] == SPACE)
		cost = space_cost;
	else cost = wall_cost;
	pneighbor = new Node(row, col, trow, tcol, pcurrent->GetG() + cost, pcurrent);
	// now add it to pq if 
	// 1. it is white
	// 2. it is gray but it improves f of this Node 
	// 3. it is "white" target
	// 4. it is "gray" target and it has f better than the previous target
	it_gray = find(grays.begin(), grays.end(), *pneighbor);
	it_black = find(blacks.begin(), blacks.end(), *pneighbor);
	// it is white if it is not black and not grays and it is not target
	if (it_gray != grays.end())// && it_black == blacks.end()) // it is gray
	{
		if (pneighbor->GetF() < (*it_gray).GetF())
		{
			UpdatePQ(pq, pneighbor);
			// update this neighbor in grays
			grays.erase(it_gray);
			grays.push_back(*pneighbor);
		}
	}
	else 	if (it_black != blacks.end())
	{
		delete(pneighbor);
	}
	else // it is white
	{
		// paint it gray
		grays.push_back(*pneighbor);
		// add it to pq
		pq.push(*pneighbor);
	}
}

void CreatePath(Node* pn)
{
	while (fabs(pn->GetG())>0.01) // only at START g=0
	{
		maze[pn->GetRow()][pn->GetCol()] = SPACE;
		pn =pn->GetParent();
	}
}

// run A* that finds the "best" path from rooms[index1] to rooms[index2]
void DigTunnel(int index1, int index2)
{
	bool target_found = false;
	int r, c, tr, tc;

	Node* pcurrent;
	vector <Node> grays;
	vector <Node> blacks;
	priority_queue <Node, vector<Node>, CompareNodes> pq;
	vector <Node>::iterator it_gray;
	// create start Node
	r = rooms[index1].GetCenterRow();
	c = rooms[index1].GetCenterCol();
	tr = rooms[index2].GetCenterRow();
	tc = rooms[index2].GetCenterCol();
	Node pn = *(new Node(r, c, tr, tc, 0, nullptr));
	// add it to pq and paint it gray
	pq.push(pn);
	grays.push_back(pn);

	while (!pq.empty() && !target_found)
	{
		// remove the top Node from pq
		pcurrent = new Node(pq.top());
		// check that it is not a target. If it is target than we just stop A*
		if(fabs(pcurrent->GetH())<0.01) // this is target
		{
			CreatePath(pcurrent);
			return;
		}
		pq.pop();
		// and paint it black
		it_gray = find(grays.begin(), grays.end(), *pcurrent);
	// remove pcurrent from grays
		if (it_gray != grays.end()) // pcurrent has been found
			grays.erase(it_gray);
		// and add it to blacks
		blacks.push_back(*pcurrent);
		// check the neighbor Nodes of pcurrent
		// check UP
		if (pcurrent->GetRow() < MSZ - 1)
		{
			CheckNeighbor(pcurrent, pcurrent->GetRow() + 1, pcurrent->GetCol(), tr, tc,grays,blacks,pq);
		}
		// check DOWN
		if (pcurrent->GetRow() > 0)
		{
			CheckNeighbor(pcurrent, pcurrent->GetRow() - 1,pcurrent->GetCol(), tr, tc, grays, blacks, pq);
		}
		// check left
		if (pcurrent->GetCol() > 0)
		{
			CheckNeighbor(pcurrent, pcurrent->GetRow() ,pcurrent->GetCol()- 1, tr, tc, grays, blacks, pq);
		}
		// check right
		if (pcurrent->GetCol() < MSZ - 1)
		{
			CheckNeighbor(pcurrent, pcurrent->GetRow(),	pcurrent->GetCol() + 1, tr, tc, grays, blacks, pq);
		}
	}
}

void DigTunnels()
{
	int i, j;

	for (i = 0; i < NUM_ROOMS; i++)
		for (j = i + 1; j < NUM_ROOMS; j++)
		{
			DigTunnel(i, j); // A*
			cout << "Tunnel from " << i << " to " << j << " is ready\n";
		}
}

void DrawMaze()
{
	int i, j;
	double sx, sy; // cell size
	double x, y;

	sx = 2.0 / MSZ;
	sy = 2.0 / MSZ;

	for(i=0;i<MSZ;i++)
		for (j = 0; j < MSZ; j++)
		{
			switch (maze[i][j])
			{
			case SPACE: 
//				glColor3d(1, 1, 1);   // white
				// counting on security map
				glColor3d(1-security_map[i][j], 1 - security_map[i][j], 1 - security_map[i][j]);
				break;
			case WALL:
				glColor3d(0.4, 0.0, 0.2);   // dark-red
				break;
			case START:
				glColor3d(0, 1, 1);   // cyan
				break;
			case TARGET:
				glColor3d(1, 0,0);   // red
				break;
			case GRAY:
				glColor3d(0, 0.6, 0);   // dark green
				break;
			case BLACK:
				glColor3d(0.6, 1, 0.8);   // green
				break;
			case PATH:
				glColor3d(0.8, 0.4, 1);   // magenta
				break;
			case HEALTH:
				glColor3d(0, 1, 0);
			}
			// draw square maze[i][j]
			x = 2 * (j / (double)MSZ) - 1;
			y = 2 * (i / (double)MSZ) - 1;

			glBegin(GL_POLYGON);  // fill up
			glVertex2d(x, y);
			glVertex2d(x, y + sy);
			glVertex2d(x + sx, y + sy);
			glVertex2d(x + sx, y);
			glEnd();
			glColor3d(0, 0, 0);
			
			/*glBegin(GL_LINE_LOOP);  // boundaries
			glVertex2d(x, y);
			glVertex2d(x, y + sy);
			glVertex2d(x + sx, y + sy);
			glVertex2d(x + sx, y);
			glEnd();*/
			
		}
}

// changes the security_map matrix
void GenerateSecurityMap()
{
	int num_explosions = 5000;
	int counter;
	int x, y;
	double  xx, yy;
	Grenade* pg1;

	for (counter = 1; counter <= num_explosions; counter++)
	{
		x = rand() % WIDTH; // x and y are random pixels
		y = rand() % HEIGHT;
		xx = (2 * x / (double)WIDTH) - 1;
		yy = (2 * y / (double)HEIGHT) - 1; // now xx,yy are in range [-1,1]
		pg1 = new Grenade(xx, yy);
		pg1->UpdateSecurityMap(maze, security_map,1.0/num_explosions);
	}


}

void display()
{
	int i;
	glClear(GL_COLOR_BUFFER_BIT); // clean frame buffer

	DrawMaze();

	if (pb != nullptr)
		pb->DrawMe();
	if (pg)
		pg->DrawMe();
	/*if (pa)
		pa->DrawA();
	if (pp)
		pp->DrawP();*/

	glutSwapBuffers(); // show all
}

void idle()
{
	if (pb && pb->IsMoving())
	{
		pb->Move(maze);
	}

	if (pg && pg->IsExploded())
		pg->Exploding(maze);

	glutPostRedisplay(); // indirect call to display
}
void menu(int choice)
{
	switch (choice)
	{
	case 1: // Fire bullet
		if (pb != nullptr)
			pb->Fire(true);
		break;
	case 2: // Throw grenade
		if (pg != nullptr)
			pg->Explode();
		break;
	case 3: // generate security map
		GenerateSecurityMap();
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		double xx, yy;
		xx = (2 * x / (double)WIDTH) - 1;
		yy = (2 * (HEIGHT - y) /(double) HEIGHT) - 1;
		pb = new Bullet(xx, yy);
		pg = new Grenade(xx, yy);
	}
}

void main(int argc, char* argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(400, 100);
	glutCreateWindow("First Example");

	glutDisplayFunc(display);
	glutIdleFunc(idle);

	glutMouseFunc(mouse);

	// menu
	glutCreateMenu(menu);
	glutAddMenuEntry("Fire bullet", 1);
	glutAddMenuEntry("Throw grenade", 2);
	glutAddMenuEntry("Create Security Map", 3);

	glutAttachMenu(GLUT_RIGHT_BUTTON);


	init();

	glutMainLoop();
	string name = "Alliance";
	alliance = new Team(name);
}
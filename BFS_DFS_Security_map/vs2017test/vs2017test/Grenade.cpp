#include "Grenade.h"
#include <math.h>
// x and y are in range [-1,1]
Grenade::Grenade(double x, double y)
{
	int i;
	double dx, dy, alpha,teta = 2*3.14/NUM_BULLETS;
	cx = x;
	cy = y;

	for (i = 0, alpha=0 ; i < NUM_BULLETS; i++,alpha+=teta)
	{
		dx = cos(alpha);
		dy = sin(alpha);
		bullets[i] = new Bullet(cx, cy, dx, dy);
		isExploded = false;
	}

}

void Grenade::Explode()
{
	int i;
	
	isExploded = true; 
	for (i = 0; i < NUM_BULLETS; i++)
		bullets[i]->Fire(true);	 
}

void Grenade::Exploding(int maze[MSZ][MSZ])
{
	int i;

	for (i = 0; i < NUM_BULLETS; i++)
	{
		if(bullets[i]->IsMoving())
			bullets[i]->Move(maze);
	}
}

void Grenade::DrawMe()
{
	int i;

	for (i = 0; i < NUM_BULLETS; i++)
		bullets[i]->DrawMe();
}

void Grenade::UpdateSecurityMap(int maze[MSZ][MSZ], double security_map[MSZ][MSZ], double delta)
{
	int i;
	bool is_moving = true;

	Explode(); // set bullets on fire
	while (is_moving)
	{
		is_moving = false;
		for (i = 0; i < NUM_BULLETS; i++)
		{
			if (bullets[i]->IsMoving())
			{
				bullets[i]->UpdateMap(maze, security_map, delta);
				is_moving = true;
			}
		}
	}

}

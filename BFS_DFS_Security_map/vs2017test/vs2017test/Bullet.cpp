#include "Bullet.h"
#include <stdlib.h>
#include <math.h>
#include "glut.h"


Bullet::Bullet(double cx, double cy, double dx, double dy)
{
	x = cx;
	y = cy;
	dirx = dx;
	diry = dy;
	isMoving = false;
}

Bullet::Bullet(double cx, double cy)  // cx,cy are coordinates in {(-1,1),(-1,1)}
{
	x = cx;
	y = cy;
	double alpha = (rand() % 360)*3.14/180; // alpha in radians
	dirx = cos(alpha);
	diry = sin(alpha);
	isMoving = false;
}

void Bullet::DrawMe()
{
	glColor3d(0, 0, 0);
	glBegin(GL_POLYGON);
	glVertex2d(x, y + 0.01);
	glVertex2d(x + 0.01, y);
	glVertex2d(x, y - 0.01);
	glVertex2d(x - 0.01, y);
	glEnd();
}

void Bullet::Move(int maze[MSZ][MSZ])
{
	int row, col;
	col = MSZ * (x + 1) / 2.0;
	row = MSZ * (y + 1) / 2.0;

	if (maze[row][col] == WALL)
		Fire(false);
	else // moving on
	{
		x += SPEED * dirx;
		y += SPEED * diry;
	}
}

void Bullet::UpdateMap(int maze[MSZ][MSZ], double security_map[MSZ][MSZ], double delta)
{
	int row, col;
	col = MSZ * (x + 1) / 2.0;
	row = MSZ * (y + 1) / 2.0;

	if (maze[row][col] == WALL)
		Fire(false);
	else // moving on
	{
		x += SPEED * dirx;
		y += SPEED * diry;
		security_map[row][col] += (delta);
	}

}

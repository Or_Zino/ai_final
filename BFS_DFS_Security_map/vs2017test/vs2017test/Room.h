#pragma once

const int MSZ = 120;
const int SPACE = 0;
const int WALL = 1;
const int START = 2;
const int TARGET = 3;
const int BLACK = 4;
const int GRAY = 5;
const int PATH = 6;
const int HEALTH = 7;

class Room
{
private:
	int width, height, centerX, centerY,row,col;
public:
	Room();
	void DrawMe();
	void SetWidth(int w) { width = w; };
	void SetHeigth(int h) { height = h; };
	void SetCenterX(int x) { centerX = x; };
	void SetCenterY(int y) { centerY = y; };
	void FillMaze(int maze[MSZ][MSZ]);
	int FillRoomP(double row, double col); //needs to be: Potion FillRoomP(double row, double col);  
	//Ammo FillRoomA(double row, double col);*/
	bool IsOverlap(int w, int h, int row, int col);
	int GetCenterRow() { return centerY; };
	int GetCenterCol() { return centerX; };
};

